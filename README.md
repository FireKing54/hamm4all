# Hamm4all

Hamm4all is a helping website created in November 2018 by @BlessedRaccoon and @Dashell.
The purpose of this website is to help new and advanced players, providing up-to-date information about The Caverns of Hammerfest, by Motion Twin. In late 2018, Eternalfest, a hack of the original game made by fans including new levels, has been released. Since then, Hamm4all is bound to contain all levels, old and new.

## 1. Installation

### 1.1 prerequisites

* Nodejs
* git
* postgreSQL
* yarn
* lerna

### 1.2 Clone

First of all, you need to clone the repository through the standard way.

### 1.3 Setup Database

#### 1.3.1 Create the database

```bash
su postgres
psql
CREATE DATABASE hamm4all WITH ENCODING utf8;
```

#### 1.3.2 Create the schema

You can either create the database schema manually or automatically. There is a common part, which is getting the base, pre-1.0 schema. You can find it in the file `packages/api/src/database/migrations/000-pre-1.0/up/schema.sql`. You just have to copy and paste all these requests in the newly created database.

##### 1.3.2.1 Manually

For the manual creation, you can go to `packages/api/src/database/migrations`. There you can see the migration directories, ordered by time. Each of them contain either an SQL file or a TypeScript file that executes the migration. You can simply copy and paste all queries that create or alter tables. **Note : this is NOT the recommended way to do this**. Automatic tools are always better.

##### 1.3.2.2 Automatically

Place yourself in the `packages/api` directory, and run the following command.
```bash
yarn run-migration
```

**Note : For this command to run correctly, you need to fill in the information about your database in your `.env` file (see part ???)**

### 1.4 Install dependencies

To install dependences, run `yarn install` in the root directory of the project

### 1.5 Setup environment

The environment in located in the `.env` file. This configuration file is not committed to git and therefore doe snot exist. You can create a template with the command `cp .env.config .env`.

The parameters begin with the database credentials.
`TEST_DB_NAME` is a the name of a "test" database, that is used in integration testing. It may change in the future. Lastly, you can define `JWT_SECRET` as the string of your chance. The more complex, the better, as it can compromise sessions if it is broken.

## 2 Run the website

To run the website, you only need to do

```bash
node index.js
```

Then, you would be able to get the site with http://localhost:8080

## 3 Link

https://hamm4all.net

## Troubleshoot

Ma requête ne parvient pas à l'API depuis le frontend !
> Regarde dans `shared`, vérifie que le token est bien là s'il y en a besoin, et que le verbe HTTP est bon !!