import { ConsoleLogger } from "@nestjs/common";

export class APILogger extends ConsoleLogger {

    protected getTimestamp(): string {
        return new Date().toISOString();
    }

}