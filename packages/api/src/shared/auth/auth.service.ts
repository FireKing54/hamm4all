import { Injectable, NotFoundException, UnauthorizedException } from "@nestjs/common";
import { UsersService } from "../../common/users/users.service";
import * as encrypt from "blessed-encrypt";
import { AuthTokenEntity } from "../../database/entities/auth-token.entity";
import Hashids from "hashids";
import { UserEntity } from "../../database/entities/users/users.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { APILogger } from "../logger/api-logger.service";

@Injectable()
export class AuthService {

    private readonly logger = new APILogger()
    public static readonly TOKEN_LENGTH = 32

    constructor(
        @InjectRepository(AuthTokenEntity) private readonly authTokenRepository: Repository<AuthTokenEntity>,
        private readonly usersService: UsersService
    ) {}

    async login(username: string, clearPassword: string) {
        const user = await this.usersService.findOneWithPassword(username);

        if (! user) {
            throw new NotFoundException("Invalid username or password");
        }
        await this.checkPasswordIsCorrect(user, clearPassword)

        const token = await this.getUserToken(user)

        return {
            token,
        };
    }

    generateToken(): string {
        const numbers: Array<number> = this.getRandomizedNumbersInArray(AuthService.TOKEN_LENGTH);
        const rawHash: string = new Hashids("AuthToken", AuthService.TOKEN_LENGTH).encode(numbers);
        return (rawHash.length > AuthService.TOKEN_LENGTH) ? rawHash.substring(0, AuthService.TOKEN_LENGTH) : rawHash;
    }

    private getRandomizedNumbersInArray(length: number = 1): Array<number> {
        const res: Array<number> = [];
        for(let i = 0 ; i < length ; i++) {
            res.push(Math.floor(Math.random() * (2 << 8)));
        }
        return res;
    }

    async createAndSaveToken(user: UserEntity) {
        const newTokenEntity = new AuthTokenEntity();
        newTokenEntity.user_id = user.id;
        newTokenEntity.token = this.generateToken()
        await this.authTokenRepository.save(newTokenEntity);
        return newTokenEntity.token;
    }

    async getUserToken(user: UserEntity) {
        const tokenEntity = await this.authTokenRepository.findOneBy({ "user_id": user.id })
        const token = tokenEntity?.token ?? await this.createAndSaveToken(user);

        return token;
    }

    async checkPasswordIsCorrect(user: UserEntity, clearPassword: string) {
        if (! await encrypt.verify(user.password, clearPassword)) {
            throw new UnauthorizedException();
        }
    }

    async signup(username: string, clearPassword: string) {
        const userAlreadyThere = await this.usersService.findOne(username);
        if (userAlreadyThere) {
            throw new UnauthorizedException(`L'utilisateur ${username} existe déjà.`);
        }

        const hashedPassword = await encrypt.getSaltedHash(clearPassword);

        const user = await this.usersService.create({
            username: username,
            password: hashedPassword
        });

        return {
            token: await this.getUserToken(user)
        };
    }

    async getTestingToken(options: {username?: string, password?: string, roleID: 1 | 2 | 3 | 4 | 5}) {
        const admin = await this.usersService.create({
            "username": options.username ?? "test",
            "password": Buffer.from(options.password ?? "test"),
        })
        admin.roleID = 5;
        await this.usersService.changeRole(admin, options.roleID)
        const token = await this.createAndSaveToken(admin)

        return token;
    }

}