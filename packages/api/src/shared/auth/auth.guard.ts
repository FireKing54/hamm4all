
import {
    CanActivate,
    ExecutionContext,
    Inject,
    UnauthorizedException,
    mixin,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Request } from 'express';
import { AuthTokenEntity } from '../../database/entities/auth-token.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { APILogger } from '../logger/api-logger.service';

export const AuthGuard = (role: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7) => {
    const logger = new APILogger("AuthGuard")

    class AuthGuard implements CanActivate {
        constructor(
            @Inject(ConfigService) public readonly configService: ConfigService,
            @InjectRepository(AuthTokenEntity) public readonly authTokenRepository: Repository<AuthTokenEntity>,
        ) { }
    
        async canActivate(context: ExecutionContext): Promise<boolean> {
            const request = context.switchToHttp().getRequest<Request>();
            const token: string = request.cookies?.["Authentication"]

            logger.verbose(`AuthToken is ${token}`)
            logger.debug(`Wanting access to ${request.method} ${request.url}, protected with role ${role}`)

            if (!token) {
                if (role === 0) {
                    return true;
                } else {
                    logger.log("Access denied, since no token was provided")
                    throw new UnauthorizedException();
                }
            }
            try {
                const userAssociatedWithToken = await this.authTokenRepository.findOne({
                    "where": {
                        "token": token
                    },
                    "relations": {
                        "user": true
                    }
                })

                if (! userAssociatedWithToken) {
                    if (role === 0) {
                        return true;
                    }
                    
                    logger.warn("Access denied, since token is invalid")
                    throw new UnauthorizedException(`Invalid token`)
                }

                request['user'] = userAssociatedWithToken.user;

                if (role > 0 && userAssociatedWithToken.user.roleID < role) {
                    throw new UnauthorizedException(`You have grade ${userAssociatedWithToken.user.roleID} but you need grade ${role} to do this action`)
                }
            } catch {
                throw new UnauthorizedException();
            }

            logger.debug(`Access granted to ${request.url}, since user has role ${request['user'].roleID} for role ${role}`)

            return true;
        }
    
        extractTokenFromHeader(request: Request): string | undefined {
            const [type, token] = request.headers.authorization?.split(' ') ?? [];
            return type === 'Bearer' ? token : undefined;
        }
    }

    const guard = mixin(AuthGuard);
    return guard;
}
