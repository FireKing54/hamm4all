import path from "path";

export const SOURCE_DIRECTORY = path.join(__dirname, "../..");
export const API_DIRECTORY = path.join(SOURCE_DIRECTORY, "..");
export const VIEWS_DIRECTORY = path.join(API_DIRECTORY, "views")
export const PACKAGES_DIRECTORY = path.join(API_DIRECTORY, "..");
export const MAIN_REPO_DIRECTORY = path.join(PACKAGES_DIRECTORY, "..");
export const CDN_DIRECTORY = path.join(MAIN_REPO_DIRECTORY, "../hamm4all-db");
export const BLOBS_DIRECTORY = path.join(CDN_DIRECTORY, "blobs");
export const ASSETS_DIRECTORY = path.join(MAIN_REPO_DIRECTORY, "assets");