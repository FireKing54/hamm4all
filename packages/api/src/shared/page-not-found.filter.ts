import {
    ArgumentsHost,
    Catch,
    ExceptionFilter,
    NotFoundException,
} from '@nestjs/common';
  
@Catch(NotFoundException)
export class PageNotFoundFilter<T extends NotFoundException>
implements ExceptionFilter {
    catch(exception: T, host: ArgumentsHost) {
        host
            .switchToHttp()
            .getResponse()
            .status(exception.getStatus())
            .send({"message": exception.message });
    }
}