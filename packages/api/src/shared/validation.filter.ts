import {
    ArgumentsHost,
    Catch,
    ExceptionFilter,
    BadRequestException,
} from '@nestjs/common';
import { APILogger } from './logger/api-logger.service';
  
@Catch(BadRequestException)
export class BadRequestFilter<T extends BadRequestException>
implements ExceptionFilter {
    private readonly logger = new APILogger(BadRequestFilter.name);

    catch(exception: T, host: ArgumentsHost) {
        const exceptionResponse = exception.getResponse()
        const message = typeof exceptionResponse === "string" ? exceptionResponse : (exceptionResponse as any).message
        this.logger.debug(`Request validation failed: ${message}`);
        host
            .switchToHttp()
            .getResponse()
            .status(exception.getStatus())
            .send({"message": message });
    }
}