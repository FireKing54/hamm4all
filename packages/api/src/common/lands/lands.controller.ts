import { Body, Controller, Get, NotFoundException, Param, Post, UseGuards } from "@nestjs/common";
import { LandsService } from "./lands.service";
import { GameParam, H4A_API, LandsAddBody, LandParam } from "@hamm4all/shared";
import { AuthGuard } from "../../shared/auth/auth.guard";

@Controller()
export class LandsController {

    constructor(
        private readonly landsService: LandsService,
    ) {}

    @Get(H4A_API.v1.lands.list.getFullRawUri())
    public async findAll(
        @Param() params: GameParam
    ) {
        const lands = await this.landsService.findAll(params.gameID);
        return lands.map(this.landsService.toLand)
    }

    @Get(H4A_API.v1.lands.get.getFullRawUri())
    public async findOne(
        @Param() params: LandParam
    ) {
        const land = await this.landsService.findOne(params.landID);

        if (!land) {
            throw new NotFoundException(`La contrée numéro "${params.landID}" n'a pas été trouvée`)
        }

        return land;
    }

    @Post(H4A_API.v1.lands.add.getFullRawUri())
    @UseGuards(AuthGuard(1))
    public async add(
        @Param() params: GameParam,
        @Body() body: LandsAddBody
    ) {
        await this.landsService.create(body, params.gameID);
    }

}