import { Injectable } from "@nestjs/common";
import { Repository, DataSource } from "typeorm";
import { GameLandEntity } from "../../database/entities/game/game-lands.entity";

interface LandCreationAttributes {
    name: string;
    game_id: number;
    description: string;
}

@Injectable()
export class GameLandsRepository extends Repository<GameLandEntity> {
    constructor(
        private dataSource: DataSource
    ) {
        super(GameLandEntity, dataSource.createEntityManager())
    }

    public createMinimal(attributes: LandCreationAttributes) {
        return this.dataSource.getRepository(GameLandEntity).create(attributes)
    }
}