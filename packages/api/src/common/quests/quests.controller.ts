import { Controller, Get } from "@nestjs/common";
import { QuestsService } from "./quests.service";
import { H4A_API, Quest } from "@hamm4all/shared";


@Controller()
export class QuestsController {

    constructor(
        private readonly questsService: QuestsService
    ) {}

    @Get(H4A_API.v1.quests.list.getFullRawUri())
    async list(): Promise<Quest[]> {
        return this.questsService.findAll();
    }

}