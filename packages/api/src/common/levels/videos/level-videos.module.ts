import { Module } from "@nestjs/common";
import { LevelVideosController } from "./level-videos.controller";
import { LevelVideosService } from "./level-videos.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { LevelVideoEntity } from "../../../database/entities/level/level-video.entity";
import { AuthTokenEntity } from "../../../database/entities/auth-token.entity";
import { ExperienceModule } from "../../users/experience/experience.module";
import { LevelsModule } from "../levels.module";

@Module({
    imports: [
        TypeOrmModule.forFeature([LevelVideoEntity, AuthTokenEntity]),
        ExperienceModule, LevelsModule
    ],
    controllers: [LevelVideosController],
    providers: [LevelVideosService]
})
export class LevelVideosModule {}