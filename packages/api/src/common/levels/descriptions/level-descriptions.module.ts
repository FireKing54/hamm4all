import { Module } from "@nestjs/common";
import { LevelDescriptionsController } from "./level-descriptions.controller";
import { LevelDescriptionsService } from "./level-descriptions.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { LevelDescriptionEntity } from "../../../database/entities/level/level-description.entity";
import { AuthTokenEntity } from "../../../database/entities/auth-token.entity";
import { LevelsModule } from "../levels.module";
import { ExperienceModule } from "../../users/experience/experience.module";


@Module({
    controllers: [LevelDescriptionsController],
    providers: [LevelDescriptionsService],
    imports: [
        TypeOrmModule.forFeature([LevelDescriptionEntity, AuthTokenEntity]),
        LevelsModule,
        ExperienceModule
    ]
})
export class LevelDescriptionsModule {}