import { Module } from "@nestjs/common";
import { LevelsService } from "./levels.service";
import { LevelsController } from "./levels.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { LevelEntity } from "../../database/entities/level/level.entity";
import { GameLandEntity } from "../../database/entities/game/game-lands.entity";
import { AuthTokenEntity } from "../../database/entities/auth-token.entity";


@Module({
    "imports": [TypeOrmModule.forFeature([LevelEntity, GameLandEntity, AuthTokenEntity])],
    "controllers": [LevelsController],
    "providers": [LevelsService],
    "exports": [LevelsService],
})
export class LevelsModule {}