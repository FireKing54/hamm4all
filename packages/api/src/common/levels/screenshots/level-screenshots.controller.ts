import { H4A_API, LEVELS_SCREENSHOT_FILE_FIELD_NAME, LevelScreenshot, LevelsParam, LevelsScreenshotOrderUpdateBody, ScreenshotParam } from "@hamm4all/shared";
import { Body, Controller, FileTypeValidator, Get, MaxFileSizeValidator, Param, ParseFilePipe, Post, Put, Req, UploadedFile, UseGuards, UseInterceptors } from "@nestjs/common";
import { AuthGuard } from "../../../shared/auth/auth.guard";
import { FileInterceptor } from "@nestjs/platform-express";
import { LevelScreenshotsService } from "./level-screenshots.service";
import { Request } from "express";
import { LevelsService } from "../levels.service";
import { ThrottlerGuard } from "@nestjs/throttler";

const levelScreenshotPipe = new ParseFilePipe({
    "validators": [
        new MaxFileSizeValidator({
            "maxSize": 1024 * 1024,
            "message": "Un screen de niveau ne doit pas dépasser 1Mo"
        }),
        new FileTypeValidator({
            "fileType": "image/png",
        })
    ]
})

@Controller()
export class LevelScreenshotsController {

    constructor(
        private readonly levelScreenshotsService: LevelScreenshotsService,
        private readonly levelsService: LevelsService
    ) {}

    @Get(H4A_API.v1.levels.screenshots.list.getFullRawUri())
    async getScreenshots(
        @Param() params: LevelsParam
    ): Promise<LevelScreenshot[]> {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)
        const screenshots = await this.levelScreenshotsService.getScreenshots(levelID);

        return screenshots.map(this.levelScreenshotsService.toLevelScreenshot);
    }

    @Get(H4A_API.v1.levels.screenshots.get.getFullRawUri())
    async getScreenshot(
        @Param() params: ScreenshotParam
    ): Promise<LevelScreenshot> {   
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)
        
        return await this.levelScreenshotsService.getScreenshot(levelID, params.screenshotOrderNumber);
    }

    @Post(H4A_API.v1.levels.screenshots.add.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.levels.screenshots.add.getRole()), ThrottlerGuard)
    @UseInterceptors(FileInterceptor(LEVELS_SCREENSHOT_FILE_FIELD_NAME))
    async addScreenshot(
        @Param() params: LevelsParam,
        @UploadedFile(levelScreenshotPipe) screenshot: Express.Multer.File,
        @Req() req: Request
    ) {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return await this.levelScreenshotsService.addScreenshot(levelID, screenshot, req.user.id);
    }

    @Put(H4A_API.v1.levels.screenshots.update.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.levels.screenshots.update.getRole()), ThrottlerGuard)
    @UseInterceptors(FileInterceptor(LEVELS_SCREENSHOT_FILE_FIELD_NAME))
    async updateScreenshot(
        @Param() params: ScreenshotParam,
        @UploadedFile(levelScreenshotPipe) screenshot: Express.Multer.File,
        @Req() req: Request
    ) {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return await this.levelScreenshotsService.updateScreenshot(levelID, params.screenshotOrderNumber, screenshot, req.user.id);
    }

    @Put(H4A_API.v1.levels.screenshots.order.update.getFullRawUri())
    @UseGuards(AuthGuard(H4A_API.v1.levels.screenshots.order.update.getRole()))
    async updateScreenshotOrder(
        @Param() params: LevelsParam,
        @Body() body: LevelsScreenshotOrderUpdateBody
    ): Promise<void> {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return await this.levelScreenshotsService.updateScreenshotOrder(levelID, body);
    }

}