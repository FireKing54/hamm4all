import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { LevelOrderEntity } from "../../../database/entities/level/level-order.entity";
import { ExperienceService } from "../../users/experience/experience.service";

@Injectable()
export class FruitOrderService {

    constructor(
        @InjectRepository(LevelOrderEntity) private readonly fruitOrderRepository: Repository<LevelOrderEntity>,
        private readonly experienceService: ExperienceService
    ) {}
 
    async getFruitOrder(screenshotID: string): Promise<LevelOrderEntity[]> {
        return this.fruitOrderRepository.find({
            "where": {
                "screenshot_id": screenshotID
            },
            "order": {
                "order_num": "ASC"
            }
        })
    }

    async updateFruitOrder(screenshotID: string, newOrder: {x: number, y: number}[], userID: number): Promise<void> {
        // Delete all the previous order
        await this.fruitOrderRepository.delete({
            "screenshot_id": screenshotID
        });

        // Create new order
        const newEntities = newOrder.map((coords, index) => {
            return this.fruitOrderRepository.create({
                "order_num": index + 1,
                "x": coords.x,
                "y": coords.y,
                "screenshot_id": screenshotID,
                "updated_by_id": userID
            });
        });

        await this.fruitOrderRepository.save(newEntities);

        // Add experience
        await this.experienceService.addExperience(userID, 8);
    }

    toFruitOrder(order: LevelOrderEntity): {x: number, y: number} {
        return {
            "x": order.x,
            "y": order.y,
        }
    }

}