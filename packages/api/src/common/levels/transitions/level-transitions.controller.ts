import { Body, Controller, Get, Param, Put, UseGuards } from "@nestjs/common";
import { LevelTransitionsService } from "./level-transitions.service";
import { H4A_API, LevelsParam, LevelTransitionsClassic, LevelTransitionsUpdateBody, LevelTransitionsLabyUpdateBody, LevelTransitionsLaby } from "@hamm4all/shared";
import { AuthGuard } from "../../../shared/auth/auth.guard";
import { LevelsService } from "../levels.service";

@Controller()
export class LevelTransitionsController {

    constructor(
        private readonly levelTransitionsService: LevelTransitionsService,
        private readonly levelsService: LevelsService
    ) {}

    @Get(H4A_API.v1.levels.transitions.classic.list.getFullRawUri())
    async getTransitions(
        @Param() params: LevelsParam,
    ): Promise<LevelTransitionsClassic> {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return this.levelTransitionsService.getClassicTransitions(levelID)
    }

    @Put(H4A_API.v1.levels.transitions.classic.update.getFullRawUri())
    @UseGuards(AuthGuard(1))
    async updateTransitions(
        @Param() params: LevelsParam,
        @Body() body: LevelTransitionsUpdateBody
    ): Promise<void> {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return this.levelTransitionsService.updateClassicTransitions(levelID, body);
    }

    @Get(H4A_API.v1.levels.transitions.laby.list.getFullRawUri())
    async getLabyTransitions(
        @Param() params: LevelsParam,
    ): Promise<LevelTransitionsLaby> {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return this.levelTransitionsService.getLabyTransitions(levelID)
    }

    @Put(H4A_API.v1.levels.transitions.laby.update.getFullRawUri())
    @UseGuards(AuthGuard(1))
    async updateLabyTransitions(
        @Param() params: LevelsParam,
        @Body() body: LevelTransitionsLabyUpdateBody
    ): Promise<void> {
        const levelID = await this.levelsService.findLevelIDFromGame(params.gameID, params.levelName)

        return this.levelTransitionsService.updateLabyTransitions(levelID, body);
    }

}