import { HttpException, Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { FindOptionsRelations, Repository } from "typeorm";
import { ItemEntity } from "../../../database/entities/items/items.entity";
import { Item, ItemsAddBody, ItemsUpdateBody } from "@hamm4all/shared";
import { ItemNameEntity } from "../../../database/entities/items/item-names.entity";
import { BlobService } from "../../../shared/blobs/blobs.service";
import { ExperienceService } from "../../users/experience/experience.service";

@Injectable()
export class ItemsService {

    private readonly logger = new Logger(ItemsService.name);

    constructor(
        @InjectRepository(ItemEntity) private readonly itemsRepository: Repository<ItemEntity>,
        private readonly blobService: BlobService,
        @InjectRepository(ItemNameEntity) private readonly itemNamesRepository: Repository<ItemNameEntity>,
        private readonly experienceService: ExperienceService
    ) {}

    async add(item: ItemsAddBody, userID: number) {

        const newItem = new ItemEntity();
        newItem.coefficient = item.coefficient;
        newItem.effect = item.effect;
        newItem.in_game_id = item.inGameID;
        newItem.type = item.effect ? 1 : 2;
        newItem.value = item.value;
        newItem.game_id = item.gameID;

        try {
            await this.itemsRepository.save(newItem);
            for(const [language, name] of Object.entries(item.names)) {
                const itemNameRow = new ItemNameEntity();
                itemNameRow.item_id = newItem.id;
                itemNameRow.language_code = language;
                itemNameRow.name = name;
                await this.itemNamesRepository.save(itemNameRow);
            }
            await this.experienceService.addExperience(userID, 15)
        }
        catch (e) {
            this.logger.error(e)
            throw new HttpException("Une erreur est survenue lors de la sauvegarde de l'item. Vérifie que l'ID interne de l'objet n'est pas utilisé par un autre objet de ce jeu.", 500);
        }
    }

    private getClassicalQueryRelationOptions(): FindOptionsRelations<ItemEntity> {
        return {
            "names": true,
            "game": true,
        }
    }

    async getAllItems(games?: Array<number>): Promise<Array<Item>> {
        console.time("getAllItemsQuery")
        const items = await this.itemsRepository.find({
            "relations": this.getClassicalQueryRelationOptions()
        })
        console.timeEnd("getAllItemsQuery")

        console.time("getAllItems")
        let itemsFiltered: any = items;
        if ((!games) || games.length === 0) {
            itemsFiltered = items;
        }
        else {
            itemsFiltered = items.filter(item => games.includes(item.game.id))
        }
        console.timeEnd("getAllItems")

        return itemsFiltered.map(this.toItem)
    }

    toItem(item: ItemEntity): Item {
        return {
            "id": item.id,
            "name": item.names.reduce((acc, elem) => {
                acc[elem.language_code] = elem.name;
                return acc;
            }, {} as {[key: string]: string}),
            "coefficient": item.coefficient,
            "in_game_id": item.in_game_id,
            "type": item.type === 1 ? "effect" : "points",
            "picture_id": item.picture_id,
            "value": item.value,
            "effect": item.effect,
            "game": {
                "id": item.game.id,
                "name": item.game.name,
                "thumbnail_id": item.game.thumbnail_id
            }
        }
    }

    async update(item: ItemsUpdateBody, id: string, userID: number) {
        const itemToUpdate = await this.itemsRepository.findOne({
            "where": {
                "id": id
            },
            "relations": this.getClassicalQueryRelationOptions()
        })

        if (!itemToUpdate) {
            throw new HttpException("Item not found", 404)
        }

        itemToUpdate.coefficient = item.coefficient;
        itemToUpdate.effect = item.effect;
        itemToUpdate.in_game_id = item.inGameID;
        itemToUpdate.type = item.effect ? 1 : 2;
        itemToUpdate.value = item.value;
        itemToUpdate.game_id = item.gameID;

        try {
            await this.itemsRepository.save(itemToUpdate);
            await this.itemNamesRepository.delete({"item_id": itemToUpdate.id})
            for(const [language, name] of Object.entries(item.names)) {
                const itemNameRow = new ItemNameEntity();
                itemNameRow.item_id = itemToUpdate.id;
                itemNameRow.language_code = language;
                itemNameRow.name = name;
                await this.itemNamesRepository.save(itemNameRow);
            }
            await this.experienceService.addExperience(userID, 10)
        }
        catch (e) {
            this.logger.error(e)
            throw new HttpException("Une erreur est survenue lors de la sauvegarde de l'item. Vérifie que l'ID interne de l'objet n'est pas utilisé par un autre objet de ce jeu.", 500);
        }
    }

    async updatePicture(itemID: string, picture: Express.Multer.File, userID: number) {
        const itemToUpdate = await this.itemsRepository.findOne({
            "where": {
                "id": itemID
            }
        })

        if (!itemToUpdate) {
            throw new HttpException("Item not found", 404)
        }

        const storedFile = await this.blobService.add({
            "data": picture.buffer,
            "mimetype": picture.mimetype,
            "size": picture.size,
        })

        itemToUpdate.picture_id = storedFile.id;

        try {
            await this.itemsRepository.save(itemToUpdate);
            await this.experienceService.addExperience(userID, 20)
        }
        catch (e) {
            this.logger.error(e)
            throw new HttpException("Une erreur est survenue lors de la sauvegarde de l'image de l'item.", 500);
        }
    }

    async delete(id: string) {
        try {
            await this.itemNamesRepository.delete({"item_id": id})
            await this.itemsRepository.delete({"id": id})
        }
        catch (e) {
            this.logger.error(e)
            throw new HttpException("Une erreur est survenue lors de la suppression de l'item.", 500);
        }
    }

}