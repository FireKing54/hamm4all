import { HttpException, Injectable } from "@nestjs/common";
import { Repository } from "typeorm";
import { GameEntity } from "../../database/entities/game/games.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Game, GamesAddBody, GamesUpdateBody } from "@hamm4all/shared";
import { BlobService } from "../../shared/blobs/blobs.service";
import { AuthorsService } from "../authors/authors.service";
import { APILogger } from "../../shared/logger/api-logger.service";
import { GameGradeEntity } from "../../database/entities/game/game-grade.entity";
import { GamesRepository } from "./games.repository";
import { GameLandsRepository } from "../lands/lands.repository";

@Injectable()
export class GamesService {

    private readonly logger = new APILogger(GamesService.name);

    constructor(
        private readonly gamesRepository: GamesRepository,
        @InjectRepository(GameGradeEntity) private readonly gameGradesRepository: Repository<GameGradeEntity>,
        private readonly gameLandRepository: GameLandsRepository,
        private readonly authorsService: AuthorsService,
        private readonly blobService: BlobService
    ) {}

    async toGameDto(gameEntity: GameEntity): Promise<Game> {
        return {
            "id": gameEntity.id,
            "has_variable_difficulty": gameEntity.has_variable_difficulty,
            "level_count": await this.gamesRepository.countLevels(gameEntity.id),
            "difficulty": gameEntity.grades.reduce((acc, {grade}) => acc + grade, 0) / gameEntity.grades.length,
            "authors": gameEntity.authors.map(author => ({
                "id": author.id,
                "name": author.name
            })),
            "description": gameEntity.description,
            "name": gameEntity.name,
            "released_at": gameEntity.released_at.toISOString(),
            "thumbnail_id": gameEntity.thumbnail_id
        }
    }

    async findAllGames() {
        const games = await this.gamesRepository.find({
            "relations": {
                "grades": true,
                "authors": true
            }
        })

        return games
    }

    async findGameById(id: number) {
        const game = await this.gamesRepository.findOne({
            "relations": {
                "grades": true,
                "authors": true
            },
            "where": {
                "id": id
            }
        })

        return game
    }

    async createGame(game: GamesAddBody, userID: number) {
        const gameEntity = new GameEntity()
        gameEntity.name = game.name
        gameEntity.description = game.description
        gameEntity.has_variable_difficulty = game.has_variable_difficulty
        gameEntity.released_at = new Date(game.released_at)
        gameEntity.updated_by_id = userID
        
        const authors = await this.authorsService.findAll()
        gameEntity.authors = authors.filter(author => game.authors.includes(author.id))

        try {
            await this.gamesRepository.save(gameEntity)
            const landEntity = this.gameLandRepository.createMinimal({
                "name": "default",
                "game_id": gameEntity.id,
                "description": `Contrée sans nom de "${game.name}". Sert aussi de contrée par défaut à la création des niveaux`
            })
            await this.gameLandRepository.save(landEntity)
        }
        catch (error) {
            this.logger.error(error)
            throw new HttpException(`Une erreur a eu lieu lors de l'insertion du jeu`, 500)
        }
    }

    async updateGame(gameID: number, game: GamesUpdateBody, userID: number) {
        const gameEntity = await this.gamesRepository.findOneBy({
            "id": gameID
        })

        if (!gameEntity) {
            throw new HttpException("Game not found", 404)
        }

        gameEntity.name = game.name
        gameEntity.description = game.description
        gameEntity.has_variable_difficulty = game.has_variable_difficulty
        gameEntity.released_at = new Date(game.released_at)
        gameEntity.updated_by_id = userID

        const authors = await this.authorsService.findAll()
        gameEntity.authors = authors.filter(author => game.authors.includes(author.id))

        await this.gamesRepository.save(gameEntity)
    }

    async updateThumbnail(gameID: number, thumbnail: Express.Multer.File, userID: number) {
        const gameEntity = await this.gamesRepository.findOneBy({
            "id": gameID
        })

        if (!gameEntity) {
            throw new HttpException("Game not found", 404)
        }

        const savedThumbnail = await this.blobService.add({
            "data": thumbnail.buffer,
            "mimetype": thumbnail.mimetype,
            "size": thumbnail.size,
        })
        gameEntity.thumbnail_id = savedThumbnail.id
        gameEntity.updated_by_id = userID
        await this.gamesRepository.save(gameEntity)
    }

    async addOrUpdateGrade(gameID: number, grade: number, userID: number) {
        const createdGrade = new GameGradeEntity()
        createdGrade.grade = grade
        createdGrade.game_id = gameID
        createdGrade.user_id = userID
        await this.gameGradesRepository.save(createdGrade)
    }

}