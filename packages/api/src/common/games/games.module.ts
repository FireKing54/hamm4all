import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { GamesController } from "./games.controller";
import { GamesService } from "./games.service";
import { BlobModule } from "../../shared/blobs/blobs.module";
import { AuthorsModule } from "../authors/authors.module";
import { AuthTokenEntity } from "../../database/entities/auth-token.entity";
import { LevelsModule } from "../levels/levels.module";
import { GameGradeEntity } from "../../database/entities/game/game-grade.entity";
import { GamesRepository } from "./games.repository";
import { GameLandsRepository } from "../lands/lands.repository";


@Module({
    "imports": [
        TypeOrmModule.forFeature([
            AuthTokenEntity,
            GameGradeEntity,
        ]),
        BlobModule,
        AuthorsModule,
        LevelsModule
    ],
    "controllers": [GamesController],
    "providers": [GamesService, GamesRepository, GameLandsRepository]
})
export class GamesModule {}