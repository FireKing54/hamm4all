import { Module } from "@nestjs/common";
import { TechniquesController } from "./techniques.controller";
import { TechniquesService } from "./techniques.service";

@Module({
    imports: [],
    controllers: [TechniquesController],
    providers: [TechniquesService],
})
export class TechniquesModule {}