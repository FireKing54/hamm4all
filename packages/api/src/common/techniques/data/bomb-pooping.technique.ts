import { Technique } from "@hamm4all/shared";
import pathConst from "../../../shared/paths/path.const";

const bombPoopingTechnique: Technique = {
    "name": "Technique du Bomb Pooping",
    "presentation": "La technique du Bomb Pooping (parfois appelée \"largage\" en français) est un glitch de jeu relativement difficile à maîtriser qui peut aider lors de certains niveaux spécifiques.",
    "description": "La technique consiste à poser une bombe tout au bord d'un plateforme en revenant sur celle-ci (cela recquiert un excellent timing). Le jeu va alors considérer que la bombe est sur la plateforme, alors que celle-ci est tout juste à côté. Cela aura pour effet de descendre la bombe tout droit, lui permettant ici d'aller plus bas qu'elle ne l'aurait été en la poussant simplement.",
    "videos": [
        [
            {
                "name": "Bomb Pooping",
                "type": "webm",
                "source": "/assets/videos/bombpooping.webm"
            }
        ]
    ],
    "utility": `Cette technique est particulièrement efficace au [10 de la crypte](${pathConst.getLevelPath(31, "54.12.1G.10")}) (vidéo au dessus), car elle permet de toucher les fruits du bas du niveau tout en restant en haut.`
}

export default bombPoopingTechnique;