import { Technique } from "@hamm4all/shared";
import pathConst from "../../../shared/paths/path.const";

const sautDeDimensionTechnique: Technique = 
{
    "name": `Les sauts de dimension`,
    "presentation": `Dans le code source du jeu, les différentes dimensions sont en réalité rassemblées en 5 "super-dimensions", séparées par des niveaux vides, et dans un ordre qui ne suit pas de logique particulière. La première est le puits. La deuxième contient les niveaux créés par DeepNight, et comporte plus de 200 niveaux. La troisième contient les niveaux créés par Ayame; tandis que la quatrième ne contient qu'un niveau de test et la cinquième une version beta du [Tombeau de Tuberculoz](${pathConst.getLevelPath(31, "54.12.1D.9.3.0")})`,
    "description": "Il est cependant théoriquement possible de dépasser le dernier niveau d'une dimension, ce qui permet d'accéder à une autre dimension. C'est ce qu'on appelle le saut de dimension. À une époque, un bug permettait de sauter entre virtuellement n'importe quelles dimensions grâce à des parapluies en mode miroir. C'est comme ça que certains joueurs comme GlobZOsiris ont réussi à récupérer le Pad Sounie de la dimension 33 sans avoir la clé du Bourru. Ce bug permettait également d'accéder à deux dimensions cachées après la dimension 72. Ce bug a depuis longtemps été corrigé, et il n'y a aujourd'hui plus que 3 sauts de dimensions possible",
    "videos": [],
    "utility": `Au [niveau 6.0](${pathConst.getLevelPath(31, "6.0")}) avec l'option de jeu "Explosifs Instables", ce qui nous emmène au [niveau 82.0](${pathConst.getLevelPath(31, "82.0")}). Au [niveau 51.10](${pathConst.getLevelPath(31, "51.10")}) en mode multi coopératif, ce qui nous emmène au [niveau 16.0](${pathConst.getLevelPath(31, "16.0")}). Ce saut nécessite cependant qu'un des deux joueurs se suicide. Cela permet toutefois de faire une partie avec les dimensions 15, 16, 25, 26, 42 et 43, ainsi que tous les niveaux du puits du niveau 17 à 51, ce qui rapporte une quantité non négligeables de points. Au [niveau 16.0](${pathConst.getLevelPath(31, "16.0")}), en ayant utilisé le saut de dimension du [niveau 51.10](${pathConst.getLevelPath(31, "51.10")}) et en entrant dans le [niveau 16.0](${pathConst.getLevelPath(31, "16.0")}) tout à gauche. On arrive ainsi au [niveau 43.0](${pathConst.getLevelPath(31, "43.0")}).`
}

export default sautDeDimensionTechnique