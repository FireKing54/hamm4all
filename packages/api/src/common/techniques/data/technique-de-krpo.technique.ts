import { Technique } from "@hamm4all/shared";
import pathConst from "../../../shared/paths/path.const";

const techniqueDeKrpoTechnique: Technique = {
    "name": "Technique de KRPO",
    "presentation": `La technique de KRPO (ou "Technique de Crapaud") est une technique inventée par le joueur [Crapaud21](${pathConst.getEFUser("96516916-1950-4605-851a-f7e623fea7c1")}) pour rendre plus facile le [1 de la vrai tombe](${pathConst.getLevelPath(31, "54.12.1G.18.1")}).\nCette technique plus compliquée à maîtriser qu'une technique classique est très ciblée et ne s'utilise que dans de très rares cas. Cependant, elle s'avère très efficace pour ce qu'elle fait.`,
    "description": `Pour cette technique, vous aurez besoin de la bombe rouge. Le principe est de lever un fruit avec la bombe rouge une première fois, puis de lui envoyer une bombe en diagonale alors qu'il est encore en l'air, afin de le propulser de manière démesurée vers le haut.`,
    "videos": [
        [
            {
                "name": "Technique de KRPO",
                "type": "webm",
                "source": "/assets/videos/techniquedeKRPO.webm"
            }
        ]
    ],
    "utility": `La technique de Crapaud est aussi utilisable pour réussir le [0.9 de Flatland](${pathConst.getLevelPath(16, "0.9")}).`
}

export default techniqueDeKrpoTechnique;