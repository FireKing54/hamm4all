import { Module } from "@nestjs/common";
import { ThumbnailLinksController } from "./thumbnail-links.controller";
import { ThumbnailLinksService } from "./thumbnail-links.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthTokenEntity } from "../../database/entities/auth-token.entity";
import { GamePartsModule } from "../game-parts/game-parts.module";
import { ThumbnailLinkEntity } from "../../database/entities/thumbnails/thumbnail-link.entity";
import { ThumbnailSequencesModule } from "../thumbnail-sequences/thumbnail-sequences.module";
import { ExperienceModule } from "../users/experience/experience.module";

@Module({
    "controllers": [ThumbnailLinksController],
    "providers": [ThumbnailLinksService],
    "exports": [ThumbnailLinksService],
    "imports": [
        TypeOrmModule.forFeature([ThumbnailLinkEntity, AuthTokenEntity]),
        ThumbnailSequencesModule,
        GamePartsModule,
        ExperienceModule
    ]
})
export class ThumbnailLinksModule {}