import { Module } from "@nestjs/common";
import { ChangelogController } from "./changelog.controller";
import { ChangelogService } from "./changelog.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ChangelogEntity } from "../../database/entities/changelog/changelog.entity";
import { AuthTokenEntity } from "../../database/entities/auth-token.entity";

@Module({
    "imports": [
        TypeOrmModule.forFeature([ChangelogEntity, AuthTokenEntity])
    ],
    "controllers": [ChangelogController],
    "providers": [ChangelogService],
    "exports": []
})
export class ChangelogModule {}