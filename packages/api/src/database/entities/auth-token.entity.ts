import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryColumn } from "typeorm";
import { UserEntity } from "./users/users.entity";

@Entity({'name': 'auth_tokens'})
export class AuthTokenEntity {

    @PrimaryColumn()
    declare user_id: number;

    @Column({
        "unique": true,
        "type": "text",
        "nullable": false
    })
    declare token: string;

    @OneToOne(() => UserEntity, user => user.id)
    @JoinColumn({name: "user_id"})
    declare user: UserEntity;

    @CreateDateColumn()
    declare created_at: Date;
}