import { Length } from "class-validator";
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { UserEntity } from "../users/users.entity";
import { GameGradeEntity } from "./game-grade.entity";
import { GameLandEntity } from "./game-lands.entity";
import { AuthorEntity } from "../authors.entity";
import { BlobEntity } from "../blobs.entity";
import { ItemEntity } from "../items/items.entity";

@Entity({name: "games"})
export class GameEntity {

    @PrimaryGeneratedColumn("identity")
    declare id: number;

    @Column("varchar", {nullable: false})
    @Length(1, 255)
    declare name: string;

    @Column("varchar", {nullable: true})
    @Length(1, 2048)
    declare description: string;

    @Column("timestamp with time zone", {nullable: false})
    declare released_at: Date;

    @UpdateDateColumn()
    declare updated_at: Date;

    @OneToOne(() => UserEntity, {"eager": true})
    @JoinColumn({name: "updated_by"})
    declare updated_by: UserEntity;

    @Column({name: "updated_by", nullable: false})
    declare updated_by_id: number;

    @Column("boolean", {nullable: false, default: false})
    declare has_variable_difficulty: boolean;

    @OneToMany(() => GameGradeEntity, (grade) => grade.game)
    declare grades: GameGradeEntity[];

    @OneToMany(() => GameLandEntity, (land) => land.game, {"cascade": true})
    declare lands: GameLandEntity[];

    @ManyToMany(() => AuthorEntity, {"cascade": true})
    @JoinTable({
        name: "game_authors",
        inverseJoinColumn: {
            name: "author_id",
            referencedColumnName: "id",
        },
        joinColumn: {
            name: "game_id",
            referencedColumnName: "id"
        }
    })
    declare authors: AuthorEntity[];

    @Column()
    declare thumbnail_id: string

    @OneToOne(() => BlobEntity)
    @JoinColumn({"name": "thumbnail_id"})
    declare thumbnail: BlobEntity

    @OneToMany(() => ItemEntity, (item) => item.game, {"cascade": true})
    declare items: ItemEntity[]
}