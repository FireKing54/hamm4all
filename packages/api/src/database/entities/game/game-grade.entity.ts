import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from "typeorm";
import { UserEntity } from "../users/users.entity";
import { Max, Min } from "class-validator";
import { GameEntity } from "./games.entity";

@Entity({name: "game_graduation"})
export class GameGradeEntity {

    @PrimaryColumn("integer")
    declare game_id: number;

    @ManyToOne(() => GameEntity)
    @JoinColumn({name: "game_id"})
    declare game: GameEntity

    @PrimaryColumn("integer")
    declare user_id: number;

    @ManyToOne(() => UserEntity)
    @JoinColumn({name: "user_id"})
    declare user: UserEntity

    @Column("integer", {nullable: false})
    @Min(0)
    @Max(10)
    declare grade: number;
}