import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { GameEntity } from "./games.entity";
import { UserEntity } from "../users/users.entity";
import { ThumbnailSequenceEntity } from "../thumbnails/thumbnail-sequence.entity";

@Entity({
    name: "game_parts",
})
export class GamePartEntity {

    @PrimaryGeneratedColumn("identity")
    declare id: number;

    @Column()
    declare name: string;

    @Column()
    declare description: string;

    @Column()
    declare game_id: number;

    @ManyToOne(() => GameEntity)
    @JoinColumn({name: "game_id"})
    declare game: GameEntity;

    @CreateDateColumn()
    declare created_at: Date;

    @UpdateDateColumn()
    declare updated_at: Date;

    @ManyToOne(() => UserEntity)
    @JoinColumn({name: "updated_by"})
    declare updated_by: UserEntity

    @Column({name: "updated_by"})
    declare updated_by_id: number

    @OneToMany(() => ThumbnailSequenceEntity, (sequence) => sequence.game_part)
    declare thumbnail_sequences: ThumbnailSequenceEntity[]
}