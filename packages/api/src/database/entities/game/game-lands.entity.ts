import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { GameEntity } from "./games.entity";
import { LevelEntity } from "../level/level.entity";

@Entity({ "name": "game_lands" })
export class GameLandEntity {
    @PrimaryGeneratedColumn("identity")
    declare id: number;

    @Column({ "type": "varchar", "length": 255, "nullable": false })
    declare name: string;

    @Column({ "type": "varchar", "length": 2048, "nullable": true })
    declare description: string;

    @OneToMany(() => LevelEntity, (level) => level.land)
    declare levels: LevelEntity[];

    @Column({ "type": "int", "nullable": false })
    declare game_id: number;

    @ManyToOne(() => GameEntity)
    @JoinColumn({ "name": "game_id" })
    declare game: GameEntity;
}