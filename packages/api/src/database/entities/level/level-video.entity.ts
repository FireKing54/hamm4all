import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { LevelEntity } from "./level.entity";
import { UserEntity } from "../users/users.entity";


@Entity({
    "name": "videos"
})
export class LevelVideoEntity {

    @PrimaryGeneratedColumn("identity")
    declare id: number;

    @Column()
    declare link: string;

    @Column()
    declare has_order: boolean;

    @Column()
    declare level_id: number;

    @OneToOne(() => LevelEntity)
    @JoinColumn({name: "level_id"})
    declare level: LevelEntity;

    @UpdateDateColumn({ "name": "updated_at" })
    declare updatedAt: Date;

    @CreateDateColumn({ "name": "created_at" })
    declare createdAt: Date;

    @OneToOne(() => UserEntity)
    @JoinColumn({ "name": "updated_by" })
    declare updated_by: UserEntity;

    @Column({ "name": "updated_by"})
    declare updatedByID: number;

    @Column({ "name": "lives_lost"})
    declare livesLost: number;
}