import { Column, CreateDateColumn, Entity, PrimaryColumn } from "typeorm";

@Entity({ "name": "changelog" })
export class ChangelogEntity {

    @PrimaryColumn()
    declare name: string;

    @CreateDateColumn({"nullable": false})
    declare date: Date;

    @Column()
    declare description: string;
}