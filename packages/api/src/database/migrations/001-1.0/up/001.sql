
CREATE TABLE Auth_Tokens (
    user_id INTEGER NOT NULL,
    token TEXT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    PRIMARY KEY(user_id),
    FOREIGN KEY(user_id) REFERENCES Users(id)
);

CREATE TABLE Changelog_Changes (
    version_name TEXT NOT NULL REFERENCES Changelog(name),
    description TEXT NOT NULL,
    PRIMARY KEY(version_name, description)
);

INSERT INTO
    Changelog_Changes (version_name, description)
        SELECT name, unnest(changes)
        FROM Changelog;

ALTER TABLE Changelog
    DROP COLUMN changes;

ALTER TABLE Changelog
    ADD COLUMN description TEXT NOT NULL DEFAULT '';

UPDATE changelog c
SET description = (
    SELECT string_agg(cc.description, '\n')
    FROM changelog_changes cc
    WHERE cc.version_name = c.name
    GROUP BY cc.version_name
);

DROP TABLE Changelog_Changes;