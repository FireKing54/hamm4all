import { UserEntity } from "./database/entities/users/users.entity";
import {} from "./shared/languages/languages.guard"

// to make the file a module and avoid the TypeScript error
export {}

declare global {
  namespace Express {
    export interface Request {
      user: UserEntity;
      language: SupportedLanguage;
    }
  }
}