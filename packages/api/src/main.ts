import { config } from "dotenv";
config({
  "path": "../../.env"
});

import { NestApplication, NestFactory } from '@nestjs/core';
import { ApiModule } from './api.module';

import { PageNotFoundFilter } from "./shared/page-not-found.filter";
import { APILogger } from "./shared/logger/api-logger.service";
import cookieParser from "cookie-parser";

async function bootstrap() {
  const api = await NestFactory.create<NestApplication>(ApiModule, {
    "logger": new APILogger()
  });

  api.use(cookieParser())
  api.useGlobalFilters(new PageNotFoundFilter());

  await api.init();
  await api.listen(8080);
}

bootstrap();
