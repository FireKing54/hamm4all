import { validateSync } from "class-validator";

export function validateObject(objectToValidate: object, ValidationClass: new () => object): boolean {
    const createdClass = new ValidationClass()
    Object.assign(createdClass, objectToValidate)
    const errors = validateSync(objectToValidate)
    return errors.length === 0;
}