export interface RandomLevel {
    level: {
        id: number;
        name: string;
    }
    gameID: number;
}