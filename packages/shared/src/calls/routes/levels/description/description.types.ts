import { ReducedUser } from "../../users/users.types";


export interface LevelDescription {
    id: number;
    content: string;
    updated_at: string;
    updated_by: ReducedUser;
}