import { IsString, Length } from "class-validator";

export class LevelsDescriptionAddBody {
    @IsString()
    @Length(0, 4096)
    declare content: string;
}