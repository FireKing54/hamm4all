import { ArrayNotEmpty, IsArray, IsString } from "class-validator";


export class LevelsScreenshotOrderUpdateBody {
    @IsArray()
    @ArrayNotEmpty()
    @IsString({ each: true })
    declare newOrderIDs: string[];
}