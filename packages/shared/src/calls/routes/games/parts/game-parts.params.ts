import { Type } from "class-transformer";
import { IsInt, Min } from "class-validator";
import { GameParam } from "../games.params";

export class GamePartParam extends GameParam {
    @IsInt()
    @Type(() => Number)
    @Min(1)
    declare gamePartIndex: number;
}