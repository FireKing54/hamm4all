import { Length } from "class-validator";


export class LandsAddBody {
    @Length(1, 255)
    declare name: string;

    @Length(0, 2048)
    declare description: string;
}