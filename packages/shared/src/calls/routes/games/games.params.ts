import { Type } from "class-transformer";
import { IsInt } from "class-validator";


export class GameParam {
    @Type(() => Number)
    @IsInt()
    declare gameID: number;
}