import { IsUUID } from "class-validator";


export class ItemParam {

    @IsUUID()
    declare itemID: string
}