import { ReducedGame } from "../games/games.types";

export interface Item {
    id: string;
    name: {
        [key: string]: string;
    }
    in_game_id: number;
    coefficient: number;
    type: "effect" | "points";
    picture_id?: string;
    value: number;
    effect?: string;
    game: ReducedGame;
}