import { IsUUID } from "class-validator";

export class FileParam {
    @IsUUID()
    id: string;
}