export interface H4AFile {
    data: string;
    contentType: string;
    size: number;
}