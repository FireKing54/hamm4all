import { H4A_API } from "@hamm4all/shared"
import { NOTIFICATION_CLASS } from "../../src/components/notifications/notification.component"

Cypress.Commands.add("startTest", () => {
    cy.clearAllLocalStorage()

    cy.then(() =>
        H4A_API.v1.auth.login.call({
            "body": {
                "username": "test7",
                "password": "test7"
            }
        })
    )
    .then((response) => {
        cy.request({
            url: "/api/v1/test/run/start",
            method: "POST",
            headers: {
                "Authorization": "Bearer " + response.data.token
            }
        })
    })
})

Cypress.Commands.add("endTest", () => {
    cy.then(() => 
        H4A_API.v1.auth.login.call({
            "body": {
                "username": "test7",
                "password": "test7"
            }
        })
    )
    .then((response) => {
        cy.request({
            url: "/api/v1/test/run/end",
            method: "POST",
            headers: {
                "Authorization": "Bearer " + response.data.token
            }
        })
    })
})

Cypress.Commands.add("isAuthenticated", (name: string) => {
    cy.get("[data-cy=logout-button]").should("exist", {"force": true})
    cy.get(`[data-cy-profile-name=${name}]`).should("exist", {"force": true})
})

Cypress.Commands.add("login", (username: string, password: string) => {
    cy.visit("/connect")
    cy.get("[data-cy=login-username]").type(username)
    cy.get("[data-cy=login-password]").type(password)
    cy.get("[data-cy=login-submit]").click()
    cy.wait(200)
})

Cypress.Commands.add("expectNotification", (type: "success" | "error") => {
    cy.get(`.${NOTIFICATION_CLASS}.${type}`).should("exist")
})

interface AddGameProps {
    name: string,
    description: string,
    releaseDate: string,
    difficulty: "variable" | "fixed",
    authors: string[]
}
Cypress.Commands.add("addGame", (props: AddGameProps) => {

    cy.login("test4", "test4")
    cy.visit("/games")
    cy.get("[data-cy=games-card-zone]").within(() => {
        cy.get("svg").click()
    })

    cy.get(".modal.is-active").within(() => {
        cy.get("[name=name]").type(props.name)
        cy.get("[name=description]").type(props.description)
        cy.get("[name=releaseDate]").type(props.releaseDate)
        cy.get("[name=difficulty]").select(props.difficulty)
        cy.get("[name=authors]").select(props.authors)

        cy.get("[type=submit]").click()
    })
})