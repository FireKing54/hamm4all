import { UserProfile } from "@hamm4all/shared"

export const LOCAL_STORAGE_SESSION_KEY = "UserSession"

export function setSession(sessionInfo: UserProfile | null) {
  localStorage.setItem(LOCAL_STORAGE_SESSION_KEY, JSON.stringify(sessionInfo))
}

export function getSession(): UserProfile | null {
  return JSON.parse(localStorage.getItem(LOCAL_STORAGE_SESSION_KEY) ?? "null")
}

export function removeSession() {
  localStorage.removeItem(LOCAL_STORAGE_SESSION_KEY)
}