import Selector, { SelectorOption } from './selector.component';
import AddedField from './added-field.component';

export interface FieldData {
    addonLabel: string;
    addonValue: string;
    value: string;
}

interface FieldSelectorProps {
    options: SelectorOption[];
    allowMultiple: boolean;
    fields: FieldData[];
    setFields: (fields: FieldData[]) => void;
}

export default function FieldSelector({ options, allowMultiple, fields, setFields }: Readonly<FieldSelectorProps>) {

    const handleSelect = (selectedOption: SelectorOption) => {
        if (selectedOption && (allowMultiple || !fields.find(field => field.addonValue === selectedOption.value))) {
            setFields([...fields, { addonLabel: selectedOption.label, addonValue: selectedOption.value, value: '' }]);
        }
    };

    const handleFieldChange = (index: number, value: string) => {
        const updatedFields = fields.map((field, idx) =>
            idx === index ? { ...field, value } : field
        );
        setFields(updatedFields);
    };

    const handleRemove = (index: number) => {
        setFields(fields.filter((_, idx) => idx !== index));
    };

    return (
        <div>
            <Selector options={options} onSelect={handleSelect} />
            {fields.map((field, index) => (
                <AddedField
                    key={index}
                    addonLabel={field.addonLabel}
                    addonValue={field.addonValue}
                    value={field.value}
                    onChange={(value) => handleFieldChange(index, value)}
                    onRemove={() => handleRemove(index)}
                />
            ))}
        </div>
    );
};
